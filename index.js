/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

function fullName(){
	let userName = prompt('Enter your fullname: ')
	console.log(userName)
}

fullName()

function age(){
	let userAge = prompt('Enter your age: ')
	console.log(userAge)
}

age()

function fullLocation(){
	let userLocation = prompt('Enter your location: ')
	console.log(userLocation)
}

fullLocation()

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
function favoriteBands(){
	console.log('1. The Beatles')
	console.log('2. Metallica')
	console.log('3. The Eagles')
	console.log('4. L arc en Ciel')
	console.log('5. Eraserheads')
}
favoriteBands()
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function theGodFather(){
	console.log('The Godfather')
	console.log('Rotten Tomatoes Rating: 97%')
}
theGodFather()

function theGodFather2(){
	console.log('The Godfather, PartII')
	console.log('Rotten Tomatoes Rating: 96%')
}
theGodFather2()

function shawshanks(){
	console.log('Shawshank Redemption')
	console.log('Rotten Tomatoes Rating: 91%')
}
shawshanks()

function mockingBird(){
	console.log('To Kill a Mockingbird')
	console.log('Rotten Tomatoes Rating: 93%')
}
mockingBird()

function psycho(){
	console.log('Psycho')
	console.log('Rotten Tomatoes Rating: 96%')
}
psycho()


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
	alert("Thank You!")
};
printFriends()